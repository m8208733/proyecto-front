#Imagen base, en docker esto es un comentario
#si importan mayus / minus
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias, solo toma las normales, es decir devDependencies NO
RUN npm install

#Puerto que expongo
EXPOSE 3000

#Comando con el que ejecuto mi aplicacion
CMD ["npm","start"]
